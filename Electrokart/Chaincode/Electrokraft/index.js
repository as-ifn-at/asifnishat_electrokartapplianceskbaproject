/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const ApplianceContract = require('./lib/appliance-contract');
const OrderContract = require('./lib/order-contract');

module.exports.ApplianceContract = ApplianceContract;
module.exports.OrderContract = OrderContract;

module.exports.contracts = [ ApplianceContract, OrderContract ];
