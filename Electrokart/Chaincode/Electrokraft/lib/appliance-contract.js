/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');
const OrderContract = require('./order-contract');

class ApplianceContract extends Contract {

    async applianceExists(ctx, applianceId) {
        const buffer = await ctx.stub.getState(applianceId);
        return (!!buffer && buffer.length > 0);
    }

    async createAppliance(
        ctx,
        applianceId,
        type,
        description,
        color,
        dateOfManufacture,
        manufactureName
    ) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'manufacturer-electrokart-com') {
            const exists = await this.applianceExists(ctx, applianceId);
            if (exists) {
                throw new Error(`The appliance ${applianceId} already exists`);
            }
            const asset = {
                type,
                description,
                color,
                dateOfManufacture,
                status: 'In Factory',
                ownedBy: manufactureName,
                assetType: 'appliance',
            };

            const buffer = Buffer.from(JSON.stringify(asset));
            await ctx.stub.putState(applianceId, buffer);

            let addApplianceEventData = { Type: 'Appliance creation', type: type };
            await ctx.stub.setEvent('addApplianceEvent', Buffer.from(JSON.stringify(addApplianceEventData)));
        } else {
            return `User under following MSP:${mspID} cannot able to perform this action`;
        }
    }

    async readAppliance(ctx, applianceId) {
        const exists = await this.applianceExists(ctx, applianceId);
        if (!exists) {
            throw new Error(`The appliance ${applianceId} does not exist`);
        }
        const buffer = await ctx.stub.getState(applianceId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async deleteAppliance(ctx, applianceId) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'manufacturer-electrokart-com') {
            const exists = await this.applianceExists(ctx, applianceId);
            if (!exists) {
                throw new Error(`The appliance ${applianceId} does not exist`);
            }
            await ctx.stub.deleteState(applianceId);

            let delApplianceEventData = { Type: 'Appliance deletion' };
            await ctx.stub.setEvent('delApplianceEvent', Buffer.from(JSON.stringify(delApplianceEventData)));
        } else {
            return `User under following MSP:${mspID} cannot able to perform this action`;
        }
    }

    async checkMatchingOrders(ctx, applianceId) {
        const exists = await this.applianceExists(ctx, applianceId);
        if (!exists) {
            throw new Error(`The Appliance ${applianceId} does not exist`);
        }

        const appBuffer = await ctx.stub.getState(applianceId);
        const appDetails = JSON.parse(appBuffer.toString());

        const queryString = {
            selector: {
                assetType: 'order',
                type: appDetails.type,
                description: appDetails.description,
                color: appDetails.color,
            },
        };

        const orderContract = new OrderContract();
        const orders = await orderContract.queryAllOrders(
            ctx,
            JSON.stringify(queryString)
        );

        return orders;
    }

    async matchOrder(ctx, applianceId, orderId) {
        const orderContract = new OrderContract();

        const appDetails = await this.readAppliance(ctx, applianceId);
        const orderDetails = await orderContract.readOrder(ctx, orderId);

        if (
            orderDetails.type === appDetails.type &&
            orderDetails.description === appDetails.description &&
            orderDetails.color === appDetails.color
        ) {
            appDetails.ownedBy = orderDetails.dealerName;
            appDetails.status = 'Assigned to a Dealer';

            const newAppBuffer = Buffer.from(JSON.stringify(appDetails));
            await ctx.stub.putState(applianceId, newAppBuffer);

            await orderContract.deleteOrder(ctx, orderId);
            return `Appliance ${applianceId} is assigned to ${orderDetails.dealerName}`;
        } else {
            return 'Order is not matching';
        }
    }

    async buyAppliance(ctx, applianceId, ownerName, billNumber) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'buyer-electrokart-com') {
            const exists = await this.applianceExists(ctx, applianceId);
            if (!exists) {
                throw new Error(`The Appliance ${applianceId} does not exist`);
            }

            const appBuffer = await ctx.stub.getState(applianceId);
            const appDetails = JSON.parse(appBuffer.toString());

            appDetails.status = `Assigned to ${ownerName} wih bill number ${billNumber}`;
            appDetails.ownedBy = ownerName;

            const newAppBuffer = Buffer.from(JSON.stringify(appDetails));
            await ctx.stub.putState(applianceId, newAppBuffer);

            return `Appliance ${applianceId} is successfully purchased by ${ownerName}`;
        } else {
            return `User under following MSP:${mspID} cannot able to perform this action`;
        }
    }

    async queryAllAppliance(ctx) {
        const queryString = {
            selector: {
                assetType: 'appliance',
            },
            sort: [{ dateOfManufacture: 'asc' }],
        };
        let resultIterator = await ctx.stub.getQueryResult(
            JSON.stringify(queryString)
        );
        let result = await this.getAllResults(resultIterator, false);
        return JSON.stringify(result);
    }

    async getApplianceByRange(ctx, startKey, endKey) {
        let resultIterator = await ctx.stub.getStateByRange(startKey, endKey);
        let result = await this.getAllResults(resultIterator, false);
        return JSON.stringify(result);
    }

    async getApplianceWithPagination(ctx, _pageSize, _bookmark) {
        const queryString = {
            selector: {
                assetType: 'appliance',
            },
        };

        const pageSize = parseInt(_pageSize, 10);
        const bookmark = _bookmark;

        const { iterator, metadata } = await ctx.stub.getQueryResultWithPagination(
            JSON.stringify(queryString),
            pageSize,
            bookmark
        );

        const result = await this.getAllResults(iterator, false);

        const results = {};
        results.Result = result;
        results.ResponseMetaData = {
            RecordCount: metadata.fetched_records_count,
            Bookmark: metadata.bookmark,
        };
        return JSON.stringify(results);
    }

    async getApplianceHistory(ctx, applianceId) {
        let resultsIterator = await ctx.stub.getHistoryForKey(applianceId);
        let results = await this.getAllResults(resultsIterator, true);
        return JSON.stringify(results);
    }

    async getAllResults(iterator, isHistory) {
        let allResult = [];

        for (
            let res = await iterator.next();
            !res.done;
            res = await iterator.next()
        ) {
            if (res.value && res.value.value.toString()) {
                let jsonRes = {};

                if (isHistory && isHistory === true) {
                    jsonRes.TxId = res.value.tx_id;
                    jsonRes.timestamp = res.value.timestamp;
                    jsonRes.Value = JSON.parse(res.value.value.toString());
                } else {
                    jsonRes.Key = res.value.key;
                    jsonRes.Record = JSON.parse(res.value.value.toString());
                }
                allResult.push(jsonRes);
            }
        }
        await iterator.close();
        return allResult;
    }

}

module.exports = ApplianceContract;
