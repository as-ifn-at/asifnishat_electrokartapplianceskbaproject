const { clientApplication } = require('./client')

let ManufacturerClient = new clientApplication()

ManufacturerClient.generateAndSubmitTxn(
    "manufacturer",
    "Admin",
    "appliancechannel",
    "Electrokraft",
    "ApplianceContract",
    "queryTxn",
    "",
    "readAppliance",
    "app-045"
).then(message => {
    console.log(message.toString())
})
