const { clientApplication } = require('./client')

let ManufacturerClient = new clientApplication()

ManufacturerClient.generateAndSubmitTxn(
    "dealer",
    "Admin",
    "appliancechannel",
    "Electrokraft",
    "OrderContract",
    "queryTxn",
    "",
    "readOrder",
    "Order-02"
).then(message => {
    console.log(message.toString())
})