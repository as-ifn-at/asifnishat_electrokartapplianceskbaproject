const { clientApplication } = require('./client')

let ManufacturerClient = new clientApplication();

ManufacturerClient.generateAndSubmitTxn(
    "manufacturer",
    "Admin",
    "appliancechannel",
    "Electrokraft",
    "ApplianceContract",
    "invokeTxn",
    "",
    "createAppliance",
    "app-045",
    "Fan",
    "4 Blades",
    "Blue",
    "21/07/2021",
    "asif"
).then(message => {
    console.log(message.toString());
})
