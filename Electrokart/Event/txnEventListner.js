const { EventListener } = require('./events')

let ManufacturerEvent = new EventListener();
ManufacturerEvent.txnEventListener("manufacturer", "Admin", "appliancechannel",
    "Electrokraft", "ApplianceContract", "createAppliance",
    "App-078", "TV", "45 inch", "Black", "11/05/2021", "Akhil");
