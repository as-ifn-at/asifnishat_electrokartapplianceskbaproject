#!/bin/bash
cd $FABRIC_CFG_PATH
# cryptogen generate --config crypto-config.yaml --output keyfiles
configtxgen -profile OrdererGenesis -outputBlock genesis.block -channelID systemchannel

configtxgen -printOrg buyer-electrokart-com > JoinRequest_buyer-electrokart-com.json
configtxgen -printOrg dealer-electrokart-com > JoinRequest_dealer-electrokart-com.json
configtxgen -printOrg manufacturer-electrokart-com > JoinRequest_manufacturer-electrokart-com.json
