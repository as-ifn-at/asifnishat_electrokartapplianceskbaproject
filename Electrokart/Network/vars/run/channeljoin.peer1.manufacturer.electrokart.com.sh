#!/bin/bash
# Script to join a peer to a channel
export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_ID=cli
export CORE_PEER_ADDRESS=192.168.68.157:7003
export CORE_PEER_TLS_ROOTCERT_FILE=/vars/keyfiles/peerOrganizations/manufacturer.electrokart.com/peers/peer1.manufacturer.electrokart.com/tls/ca.crt
export CORE_PEER_LOCALMSPID=manufacturer-electrokart-com
export CORE_PEER_MSPCONFIGPATH=/vars/keyfiles/peerOrganizations/manufacturer.electrokart.com/users/Admin@manufacturer.electrokart.com/msp
export ORDERER_ADDRESS=192.168.68.157:7009
export ORDERER_TLS_CA=/vars/keyfiles/ordererOrganizations/electrokart.com/orderers/orderer1.electrokart.com/tls/ca.crt
if [ ! -f "appliancechannel.genesis.block" ]; then
  peer channel fetch oldest -o $ORDERER_ADDRESS --cafile $ORDERER_TLS_CA \
  --tls -c appliancechannel /vars/appliancechannel.genesis.block
fi

peer channel join -b /vars/appliancechannel.genesis.block \
  -o $ORDERER_ADDRESS --cafile $ORDERER_TLS_CA --tls
