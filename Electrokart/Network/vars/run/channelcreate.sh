#!/bin/bash
# Script to create channel block 0 and then create channel
cp $FABRIC_CFG_PATH/core.yaml /vars/core.yaml
cd /vars
export FABRIC_CFG_PATH=/vars
configtxgen -profile OrgChannel \
  -outputCreateChannelTx appliancechannel.tx -channelID appliancechannel

export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_ID=cli
export CORE_PEER_ADDRESS=192.168.68.157:7003
export CORE_PEER_TLS_ROOTCERT_FILE=/vars/keyfiles/peerOrganizations/manufacturer.electrokart.com/peers/peer1.manufacturer.electrokart.com/tls/ca.crt
export CORE_PEER_LOCALMSPID=manufacturer-electrokart-com
export CORE_PEER_MSPCONFIGPATH=/vars/keyfiles/peerOrganizations/manufacturer.electrokart.com/users/Admin@manufacturer.electrokart.com/msp
export ORDERER_ADDRESS=192.168.68.157:7011
export ORDERER_TLS_CA=/vars/keyfiles/ordererOrganizations/electrokart.com/orderers/orderer3.electrokart.com/tls/ca.crt
peer channel create -c appliancechannel -f appliancechannel.tx -o $ORDERER_ADDRESS \
  --cafile $ORDERER_TLS_CA --tls
