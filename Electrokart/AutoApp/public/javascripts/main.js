
function toManuDash() {
    window.location.href = '/manufacturer';
}

function swalBasic(data) {
    swal.fire({
        // toast: true,
        icon: `${data.icon}`,
        title: `${data.title}`,
        animation: true,
        position: 'center',
        showConfirmButton: true,
        footer: `${data.footer}`,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', swal.stopTimer)
            toast.addEventListener('mouseleave', swal.resumeTimer)
        }
    })
}

// function swalDisplay(data) {
//     swal.fire({
//         // toast: true,
//         icon: `${data.icon}`,
//         title: `${data.title}`,
//         animation: false,
//         position: 'center',
//         html: `<h3>${JSON.stringify(data.response)}</h3>`,
//         showConfirmButton: true,
//         timer: 3000,
//         timerProgressBar: true,
//         didOpen: (toast) => {
//             toast.addEventListener('mouseenter', swal.stopTimer)
//             toast.addEventListener('mouseleave', swal.resumeTimer)
//         }
//     }) 
// }

function reloadWindow() {
    window.location.reload();
}

function ManWriteData() {
    event.preventDefault();
    const applianceId = document.getElementById('applianceId').value;
    const type = document.getElementById('type').value;
    const description = document.getElementById('description').value;
    const color = document.getElementById('color').value;
    const dateOfManufacture = document.getElementById('dateOfManufacture').value;
    const manufactureName = document.getElementById('manufactureName').value;
    console.log(type + description + color);

    if (applianceId.length == 0 || type.length == 0 || description.length == 0 || color.length == 0 || dateOfManufacture.length == 0 || manufactureName.length == 0) {
        const data = {
            title: "You might have missed something",
            footer: "Enter all mandatory fields to add a new appliance",
            icon: "warning"
        }
        swalBasic(data);
    }
    else {
        fetch('/manuwrite', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ applianceId: applianceId, type: type, description: description, color: color, dateOfManufacture: dateOfManufacture, manufactureName: manufactureName })
        })
            .then(function (response) {
                if (response.status == 200) {
                    const data = {
                        title: "Success",
                        footer: "Added a new appliance",
                        icon: "success"
                    }
                    swalBasic(data);
                } else {
                    const data = {
                        title: `Appliance with Number ${applianceId} already exists`,
                        footer: "Vin Number must be unique",
                        icon: "error"
                    }
                    swalBasic(data);
                }

            })
            .catch(function (error) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);
            })
    }
}
function ManQueryData() {

    event.preventDefault();
    const applianceId = document.getElementById('applianceId').value;

    console.log(applianceId);

    if (applianceId.length == 0) {
        const data = {
            title: "Enter a Valid Appliance Number",
            footer: "This is a mandatory field",
            icon: "warning"
        }
        swalBasic(data)
    }
    else {
        fetch('/manuread', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ applianceId: applianceId })
        })
            .then(function (response) {
                console.log(response);
                return response.json();
            })
            .then(function (Appliancedata) {
                dataBuf = Appliancedata["Appliancedata"]
                swal.fire({
                    // toast: true,
                    icon: `success`,
                    title: `Current status of Appliance with Qvin ${applianceId} :`,
                    animation: false,
                    position: 'center',
                    html: `<h3>${dataBuf}</h3>`,
                    showConfirmButton: true,
                    timer: 3000,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', swal.stopTimer)
                        toast.addEventListener('mouseleave', swal.resumeTimer)
                    }
                })
            })
            .catch(function (error) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);
            })
    }
}

//Method to get the history of an item
function getItemHistory(applianceId) {
    console.log("postalId", applianceId)
    window.location.href = '/itemhistory?applianceId=' + applianceId;
}

function getMatchingOrders(applianceId) {
    console.log("applianceId", applianceId)
    window.location.href = 'matchOrder?applianceId=' + applianceId;
}

function buyAppliance() {
    console.log("Entered the register function")
    event.preventDefault();
    const applianceId = document.getElementById('applianceId').value;
    const ownerName = document.getElementById('ownerName').value;
    const billNumber = document.getElementById('billNumber').value;
    console.log(applianceId + ownerName + billNumber);

    if (applianceId.length == 0 || ownerName.length == 0 || billNumber.length == 0) {
        const data = {
            title: "You have missed something",
            footer: "All fields are mandatory",
            icon: "warning"
        }
        swalBasic(data)
    }
    else {
        fetch('/buyAppliance', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ applianceId: applianceId, ownerName: ownerName, billNumber: billNumber })
        })
            .then(function (response) {
                if (response.status === 200) {
                    const data = {
                        title: `Purchased appliance ${applianceId} to ${ownerName}`,
                        footer: "Purchased appliance",
                        icon: "success"
                    }
                    swalBasic(data)
                } else {
                    const data = {
                        title: `Failed to purchase`,
                        footer: "Please try again !!",
                        icon: "error"
                    }
                    swalBasic(data)
                }
            })
            .catch(function (err) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);
            })
    }
}

function createOrder() {
    console.log("Entered the order function")
    event.preventDefault();
    const orderId = document.getElementById('orderId').value;
    const type = document.getElementById('type').value;
    const description = document.getElementById('description').value;
    const color = document.getElementById('color').value;
    const dealerName = document.getElementById('dealerName').value;
    console.log(orderId + color + dealerName);

    if (orderId.length == 0 || type.length == 0 || description.length == 0
        || color.length == 0 || dealerName.length == 0) {
        const data = {
            title: "You have missed something",
            footer: "All fields are mandatory",
            icon: "warning"
        }
        swalBasic(data)
    }
    else {
        fetch('/createOrder', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ orderId: orderId, type: type, description: description, color: color, dealerName: dealerName })
        })
            .then(function (response) {
                if (response.status === 200) {
                    const data = {
                        title: `Order is created`,
                        footer: "Raised Order",
                        icon: "success"
                    }
                    swalBasic(data)

                } else {
                    const data = {
                        title: `Failed to create order`,
                        footer: "Please try again !!",
                        icon: "error"
                    }
                    swalBasic(data)
                }
            })
            .catch(function (err) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);
            })
    }
}

function readOrder() {
    console.log("Entered the order function")
    event.preventDefault();
    const orderId = document.getElementById('orderId').value;

    console.log(orderId);

    if (orderId.length == 0) {
        const data = {
            title: "Enter a order number",
            footer: "Order Number is mandatory",
            icon: "warning"
        }
        swalBasic(data)
    }
    else {
        fetch('/readOrder', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ orderId: orderId })
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (orderData) {
                dataBuf = orderData["orderData"]
                swal.fire({
                    // toast: true,
                    icon: `success`,
                    title: `Current status of Order : `,
                    animation: false,
                    position: 'center',
                    html: `<h3>${dataBuf}</h3>`,
                    showConfirmButton: true,
                    timer: 3000,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', swal.stopTimer)
                        toast.addEventListener('mouseleave', swal.resumeTimer)
                    }
                })
            })
            .catch(function (err) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);
            })
    }
}

function matchOrder(orderId, applianceId) {
    if (!orderId || !applianceId) {
        const data = {
            title: "Enter a order number",
            footer: "Order Number is mandatory",
            icon: "warning"
        }
        swalBasic(data)
    } else {
        fetch('/match', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ orderId, applianceId })
        })
            .then(function (response) {
                if (response.status === 200) {
                    const data = {
                        title: `Order matched successfully`,
                        footer: "Order matched",
                        icon: "success"
                    }
                    swalBasic(data)
                } else {
                    const data = {
                        title: `Failed to match order`,
                        footer: "Please try again !!",
                        icon: "error"
                    }
                    swalBasic(data)
                }
            })

            .catch(function (err) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);
            })
    }
}


function allOrders() {
    window.location.href = '/allOrders';
}


function getEvent() {
    fetch('/event', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(function (response) {
            console.log("Response is ###", response)
            return response.json()
        })
        .then(function (event) {
            dataBuf = event["applianceEvent"]
            swal.fire({
                toast: true,
                // icon: `${data.icon}`,
                title: `Event : `,
                animation: false,
                position: 'top-right',
                html: `<h5>${dataBuf}</h5>`,
                showConfirmButton: false,
                timer: 5000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', swal.stopTimer)
                    toast.addEventListener('mouseleave', swal.resumeTimer)
                }
            })
        })
        .catch(function (err) {
            swal.fire({
                toast: true,
                icon: `error`,
                title: `Error`,
                animation: false,
                position: 'top-right',
                showConfirmButton: true,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', swal.stopTimer)
                    toast.addEventListener('mouseleave', swal.resumeTimer)
                }
            })
        })
}