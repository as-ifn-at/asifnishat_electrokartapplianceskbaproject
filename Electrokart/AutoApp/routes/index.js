var express = require('express');
var router = express.Router();
const {clientApplication} = require('./client');
const {Events} = require('./events')
let eventClient = new Events()
eventClient.contractEventListner("manufacturer", "Admin", "appliancechannel",
"Electrokraft", "ApplianceContract", "addApplianceEvent")



/* GET home page. */
router.get('/', function(req, res, next) {
  let buyerClient = new clientApplication();
 
  buyerClient.generatedAndEvaluateTxn(
      "buyer",
      "Admin",
      "appliancechannel", 
      "Electrokraft",
      "ApplianceContract",
      "queryAllAppliance"
  )
  .then(appliances => {
    const dataBuffer = appliances.toString();
    console.log("appliances are ", appliances.toString())
    const value = JSON.parse(dataBuffer)
    console.log("History DataBuffer is",value)
    res.render('index', { title: 'Appliances Consortium', itemList: value});
  }).catch(err => {
    res.render("error", {
      message: `Some error occured`,
      callingScreen: "error",
    })
  })
});
 
router.get('/manufacturer', function(req, res, next) {
  let manufacturerClient = new clientApplication();
  manufacturerClient.generatedAndEvaluateTxn(
    "manufacturer",
    "Admin",
    "appliancechannel",
    "Electrokraft",
    "ApplianceContract",
    "queryAllAppliance"
  ).then(appliances =>{
    const data =appliances.toString();
    const value = JSON.parse(data)
    res.render('manufacturer', { title: 'Manufacturer Dashboard', itemList: value });
  }).catch(err => {
    res.render("error", {
      message: `Some error occured`,
      callingScreen: "error",
    })
  })

});
router.get('/dealer', function(req, res, next) {
  res.render('dealer', { title: 'Dealer Dashboard' });
});

router.get('/event', function(req, res, next) {
  console.log("Event Response %%%$$^^$%%$",eventClient.getEvents().toString())
  var event = eventClient.getEvents().toString()
  res.send({applianceEvent: event})
  // .then(array => {
  //   console.log("Value is #####", array)
  //   res.send(array);
  // }).catch(err => {
  //   console.log("errors are ", err)
  //   res.send(err)
  // })
  // res.render('index', { title: 'Dealer Dashboard' });
});


router.get('/buyer', function(req, res, next) {
  let buyerClient = new clientApplication();
  buyerClient.generatedAndEvaluateTxn(
    "buyer",
    "Admin",
    "appliancechannel", 
    "Electrokraft",
    "ApplianceContract",
    "queryAllAppliance"
  )
  .then(appliances => {
    const dataBuffer = appliances.toString();
    console.log("Appliances are ", appliances.toString())
    const value = JSON.parse(dataBuffer)
    console.log("History DataBuffer is",value)
    res.render('buyer', { title: 'Buyer Dashboard', itemList: value});
  }).catch(err => {
    res.render("error", {
      message: `Some error occured`,
      callingScreen: "error",
    })
})});



router.get('/addApplianceEvent', async function(req, res, next) {
  let buyerClient = new clientApplication();
  const result = await buyerClient.contractEventListner("manufacturer", "Admin", "appliancechannel", 
  "Electrokraft", "addApplianceEvent")
  console.log("The result is ####$$$$",result)
  res.render('manufacturer', {view: "applianceEvents", results: result })
})

router.post('/manuwrite',function(req,res){

  const applianceId = req.body.applianceId;
  const type = req.body.type;
  const description = req.body.description;
  const color = req.body.color;
  const dateOfManufacture = req.body.dateOfManufacture;
  const manufactureName = req.body.manufactureName;

  // console.log("Request Object",req)
  let ManufacturerClient = new clientApplication();
  
  ManufacturerClient.generatedAndSubmitTxn(
      "manufacturer",
      "Admin",
      "appliancechannel", 
      "Electrokraft",
      "ApplianceContract",
      "createAppliance",
      applianceId,type,description,color,dateOfManufacture,manufactureName
    ).then(message => {
        console.log("Message is $$$$",message)
        res.status(200).send({message: "Added Appliance"})
      }
    )
    .catch(error =>{
      console.log("Some error Occured $$$$###", error)
      res.status(500).send({error:`Failed to Add`,message:`${error}`})
    });
});

router.post('/manuread',async function(req,res){
  const applianceId = req.body.applianceId;
  let ManufacturerClient = new clientApplication();
  
  ManufacturerClient.generatedAndEvaluateTxn( 
    "manufacturer",
    "Admin",
    "appliancechannel", 
    "Electrokraft",
    "ApplianceContract",
    "readAppliance", applianceId)
    .then(message => {
      
      res.status(200).send({ Appliancedata : message.toString() });
    }).catch(error =>{
     
      res.status(500).send({error:`Failed to Add`,message:`${error}`})
    });

 })

 //  Get History of a car
 router.get('/itemhistory',async function(req,res){
  const applianceId = req.query.applianceId;
 
  let buyerClient = new clientApplication();
  
  buyerClient.generatedAndEvaluateTxn( 
    "manufacturer",
    "Admin",
    "appliancechannel", 
    "Electrokraft",
    "ApplianceContract",
    "getApplianceHistory", applianceId).then(message => {
    const dataBuffer = message.toString();
    
    const value = JSON.parse(dataBuffer)
    res.render('history', { itemList: value , title: "Appliance History"})

  });

 })


 router.post('/buyAppliance',async function(req,res){
  const applianceId = req.body.applianceId;
  const ownerName = req.body.ownerName;
  const billNumber = req.body.billNumber;
  let buyerClient = new clientApplication();
  
  buyerClient.generatedAndSubmitTxn( 
    "buyer",
    "Admin",
    "appliancechannel", 
    "Electrokraft",
    "ApplianceContract",
    "buyAppliance", applianceId,ownerName,billNumber)
    .then(message => {
    
      res.status(200).send("Successfully created")
    }).catch(error =>{
     
      res.status(500).send({error:`Failed to create`,message:`${error}`})
    });

 })
// Create order
router.post('/createOrder',async function(req,res){
  const orderId = req.body.orderId;
  const type = req.body.type;
  const description = req.body.description;
  const color = req.body.color;
  const dealerName = req.body.dealerName
  let DealerClient = new clientApplication();

  const transientData = {
    type: Buffer.from(type),
    description: Buffer.from(description),
    color: Buffer.from(color),
    dealerName: Buffer.from(dealerName)
  }
  
  DealerClient.generatedAndSubmitPDC( 
    "dealer",
    "Admin",
    "appliancechannel", 
    "Electrokraft",
    "OrderContract",
    "createOrder", orderId,transientData)
    .then(message => {
      
      res.status(200).send("Successfully created")
    }).catch(error =>{
     
      res.status(500).send({error:`Failed to create`,message:`${error}`})
    });

 })

 router.post('/readOrder',async function(req,res){
  const orderId = req.body.orderId;
  let DealerClient = new clientApplication();
  DealerClient.generatedAndEvaluateTxn( 
    "dealer",
    "Admin",
    "appliancechannel", 
    "Electrokraft",
    "OrderContract",
    "readOrder", orderId).then(message => {
   
    res.send({orderData : message.toString()});
  }).catch(error => {
    alert('Error occured')
  })

 })

 //Get all orders
 router.get('/allOrders',async function(req,res){
  let DealerClient = new clientApplication();
  DealerClient.generatedAndEvaluateTxn( 
    "dealer",
    "Admin",
    "appliancechannel", 
    "Electrokraft",
    "OrderContract",
    "queryAllOrders","").then(message => {
    const dataBuffer = message.toString();
    const value = JSON.parse(dataBuffer);
    res.render('orders', { itemList: value , title: "All Orders"})
    }).catch(error => {
    //alert('Error occured')
    console.log(error)
  })

 })
 //Find matching orders
 router.get('/matchOrder',async function(req,res){
  const applianceId = req.query.applianceId;
 
  let buyerClient = new clientApplication();
  
  buyerClient.generatedAndEvaluateTxn( 
    "manufacturer",
    "Admin",
    "appliancechannel", 
    "Electrokraft",
    "ApplianceContract",
    "checkMatchingOrders", applianceId).then(message => {
    console.log("Message response",message)
    var dataBuffer = message.toString();
    var data =[];
    data.push(dataBuffer,applianceId)
    console.log("checkMatchingOrders",data)
    const value = JSON.parse(dataBuffer)
    let array = [];
    if(value.length) {
        for (i = 0; i < value.length; i++) {
            array.push({
               "orderId": `${value[i].Key}`,"applianceId":`${applianceId}`,
                "type": `${value[i].Record.type}`, "description":`${value[i].Record.description}`, 
                "color":`${value[i].Record.color}`, 
                "dealerName": `${value[i].Record.dealerName}`,"assetType": `${value[i].Record.assetType}`
            })
        }
    }
    console.log("Array value is ", array)
    console.log("Appliance id sent",applianceId)
    res.render('matchOrder', { itemList: array , title: "Matching Orders"})

  });

 })

 router.post('/match',async function(req,res){
  const orderId = req.body.orderId;
  const applianceId = req.body.applianceId;
  let DealerClient = new clientApplication();
  DealerClient.generatedAndSubmitTxn( 
    "dealer",
    "Admin",
    "appliancechannel", 
    "Electrokraft",
    "ApplianceContract",
    "matchOrder", applianceId,orderId).then(message => {
   
      res.status(200).send("Successfully Matched order")
    }).catch(error =>{
     
      res.status(500).send({error:`Failed to Match Order`,message:`${error}`})
    });

 })



module.exports = router;

