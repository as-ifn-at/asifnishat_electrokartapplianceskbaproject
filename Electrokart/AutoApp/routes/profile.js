let profile = {
    dealer: {
        "Wallet":"../Network/vars/profiles/vscode/wallets/dealer.electrokart.com",
        "CP": "../Network/vars/profiles/appliancechannel_connection_for_nodesdk.json"
    },
    buyer : {
        "Wallet":"../Network/vars/profiles/vscode/wallets/buyer.electrokart.com",
        "CP": "../Network/vars/profiles/appliancechannel_connection_for_nodesdk.json"
    },
    manufacturer:{
        "Wallet":"../Network/vars/profiles/vscode/wallets/manufacturer.electrokart.com",
        "CP": "../Network/vars/profiles/appliancechannel_connection_for_nodesdk.json"
    }


} 

module.exports = {
    profile
}
